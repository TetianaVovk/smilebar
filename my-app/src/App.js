import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { red } from '@material-ui/core/colors';
import PaperComponent from './Components/PaperComponent';
import { Consts } from './Constans';
import GoogleMap from './Components/GoogleMap';
// import OSM from './Components/OSMComponent';

const styles = theme => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
    textColor: red,
  }
});

class App extends Component {
  state = {
    value: 0,
  };
  
  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() { 
  const { classes } = this.props;
  const { value } = this.state;

    return (
      <div  className={classes.root}>
        <AppBar position="static" color="default">
          <Tabs
            value={value}
            onChange={this.handleChange}
            variant="fullWidth"
            scrollButtons="auto"
            textColor="secondary"
          >
            <Tab label="Головна" />
            <Tab label="Процедура" />
            <Tab label="Ціна" />
            <Tab label="Контакти" />
            <Tab label="Про нас"/>
          </Tabs>
        </AppBar>
        {value === 0 && <PaperComponent text={Consts.TabT} />}
        {value === 1 && <PaperComponent header={Consts.TabH1} text={Consts.TabT1} />}
        {value === 2 && <PaperComponent header={Consts.TabH2} text={Consts.TabT2} />}
        {value === 3 && <PaperComponent header={Consts.TabH3} text={Consts.TabT3} MapComponent={GoogleMap} />}
        {value === 4 && <PaperComponent header={Consts.TabH4} text={Consts.TabT4} />}
      </div>
    );
  }
}
export default withStyles(styles)(App);




