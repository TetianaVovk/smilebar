import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import marker from '../imgs/marker.png';

const markerStyle = {
  width: 40,
  transform: 'translateY(-100%)'
  
}
const SmileBar = () => (
  <div>
    <img src={marker} style={markerStyle} />
  </div>
);
 
class GoogleMap extends Component {
  static defaultProps = {
    center: {
      lat: 49.837961,
      lng: 24.035339
    },
    zoom: 17
  };
 
  render() {
    return (
      <div style={{ height: '100vh', width: '100%' }}>
        <GoogleMapReact
          defaultCenter={this.props.center}
          defaultZoom={this.props.zoom}
          bootstrapURLKeys={{ key: 'AIzaSyCF9vmH09NGzzECNWT8cd4mdmHH69i7vFY' }}
        >
          <SmileBar
            lat={49.837961}
            lng={24.035339}
            text={'Smile Bar'}
          />
        </GoogleMapReact>
      </div>
    );
  }
}
 
export default GoogleMap;