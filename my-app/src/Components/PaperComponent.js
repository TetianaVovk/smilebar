import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
  },
});

class PaperComponent extends Component {
render(){
  const { classes, header, text, MapComponent, ref } = this.props;

  return (
    <div ref={ref}>
      <Paper className={classes.root} elevation={1}>
        <Typography variant="h5" component="h3" align="center" >
         <div style={{fontFamily:'cursive'}}>{header}</div>
        </Typography>
        <Typography component="p" align="center">
        <div style={{fontFamily:'cursive'}}>{text}</div>
        </Typography>
      </Paper>
      {MapComponent && <MapComponent />}
    </div>
  );
}
}


export default withStyles(styles)(PaperComponent);